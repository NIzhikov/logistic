import sbt._
import Keys._

object LogisticBuild extends Build {
    lazy override val projects = Seq(root)
    lazy val root = Project("root", file(".")) settings(
        commands ++= Seq(startneo, stopneo, cleardata, backupdata, restoredata)
    )

    val geotoolsVersion = SettingKey[String]("11.0", "Geo tools version")

    def startneo = Command.command("startneo") { s => ("neo4j start" !); s }
    def stopneo = Command.command("stopneo") { s => ("neo4j stop" !); s }
    def cleardata = Command.command("cleardata") { s => ("rm -R /usr/local/share/neo4j/data/graph.db" !); s }
    def backupdata = Command.command("backupdata") { s =>
        ("rm -R /usr/local/share/neo4j/data/graph.db.backup" !)
        ("cp -R /usr/local/share/neo4j/data/graph.db /usr/local/share/neo4j/data/graph.db.backup" !);
        s
    }
    def restoredata = Command.command("restoredata") { s =>
        ("rm -R /usr/local/share/neo4j/data/graph.db" !)
        ("cp -R /usr/local/share/neo4j/data/graph.db.backup /usr/local/share/neo4j/data/graph.db" !)
        s
    }
}
