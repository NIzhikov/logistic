name := "logistic"

version := "0.0.2"

scalaVersion := "2.10.3"

geotoolsVersion := "11.0"

resolvers += "Open Source Geospatial Foundation Repository" at "http://download.osgeo.org/webdav/geotools/"

resolvers += "OpenGeo Maven Repository" at "http://repo.opengeo.org"

resolvers += "twttr" at "http://maven.twttr.com"

resolvers += "Akka Repository" at "http://repo.akka.io/releases/"

transitiveClassifiers in Global := Seq(Artifact.SourceClassifier)

libraryDependencies <+= geotoolsVersion("org.geotools" % "gt-geometry" % _)

libraryDependencies <+= geotoolsVersion("org.geotools" % "gt-main" % _)

libraryDependencies <+= geotoolsVersion("org.geotools" % "gt-api" % _)

libraryDependencies <+= geotoolsVersion("org.geotools" % "gt-shapefile" % _)

libraryDependencies <+= geotoolsVersion("org.geotools" % "gt-swing" % _)

libraryDependencies <+= geotoolsVersion("org.geotools" % "gt-render" % _)

libraryDependencies ++= Seq(
    "com.typesafe.slick" % "slick_2.10" % "1.0.1",
    "org.neo4j" % "neo4j" % "2.1.4",
    "net.sourceforge.jtds" % "jtds" % "1.3.1",
    "com.twitter" % "finatra_2.10" % "1.5.3",
    "com.github.nscala-time" % "nscala-time_2.10" % "1.2.0",
    "org.slf4j" % "slf4j-simple" % "1.7.1",
    "junit" % "junit" % "4.10" % "test",
    "org.scalatest" % "scalatest_2.10" % "1.9.1" % "test",
    "com.typesafe.scala-logging" % "scala-logging-slf4j_2.10" % "2.1.2",
    "org.apache.spark" % "spark-core_2.10" % "1.0.2"
)

fork := true

javaOptions in run += "-Xmx5G"
