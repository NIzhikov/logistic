package ru.dragon.logistic

import com.vividsolutions.jts.geom.Coordinate
import org.neo4j.cypher.ExecutionEngine
import org.neo4j.graphdb._
import org.neo4j.graphdb.factory.GraphDatabaseFactory
import org.neo4j.graphdb.traversal.TraversalDescription
import ru.dragon.logistic.helpers.{CoordinateConverter, Geometry}

import scala.collection.JavaConverters._
import scala.collection.immutable.HashMap

object GraphDB {
    val GRAPH_DB = "/usr/local/share/neo4j/data/graph.db"

    val graphDB = {
        val graphDB = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(GRAPH_DB).
          loadPropertiesFromFile("/usr/local/share/neo4j/conf/neo4j.properties").
          newGraphDatabase()
        sys.addShutdownHook { graphDB.shutdown }
        graphDB
    }

    def apply() = graphDB
}

trait GraphObjects extends CoordinateConverter with Geometry {
    def execute(query: String)(implicit db: GraphDatabaseService) = new ExecutionEngine(db).execute(query)

    def execute(query: String, params: Map[String, Any])(implicit db: GraphDatabaseService) = new ExecutionEngine(db).execute(query, params)

    /**
     * Узел дорожного графа
     */
    val POINT = new Label { override def name = "POINT" }

    /**
     * Точка, описывающая геодезическую форму дороги
     */
    val GEO_POINT = new Label { override def name = "GEO_POINT" }

    /**
     * Точка, разделяющая длиный отрезок дороги на мелкие
     */
    val DIVIDE_POINT = new Label { override def name = "DIVIDE_POINT" }

    /**
     * Точка привязок SITE к дорожной сети. Перпендикуляр
     */
    val NORMAL_POINT = new Label { override def name = "NORMAL_POINT" }

    /**
     * Ребро дорожного графа
     */
    val ROAD =  new RelationshipType { override def name = "ROAD"}

    /**
     * Ребро дорожного графа, для обозначения дорог, по которым можно ехать в обоих направлениях. ONEWAY = B
     */
    val REVERSE_ROAD =  new RelationshipType { override def name = "REVERSE_ROAD"}

    /**
     * Ребро геодезиское, отражает геометрическую струтктуру ребра(с закруглениями и изгибами)
     */
    val GEO_ROAD = new RelationshipType { override def name = "GEO_ROAD" }

    /**
     * часть дороги от GEO_POINT до точки перпендикуляра NORMAL_POINT
     */
    val TO_NORMAL = new RelationshipType { override def name(): String = "TO_NORMAL" }

    /**
     * Конструкция
     */
    val SITE = new Label { override def name = "SITE" }

    /**
     * Сторона конструкции. Имеет GID и т.п.
     */
    val BILLBOARD = new Label { override def name = "BILLBOARD" }

    /**
     * BILLBOARD находится(оносится, висит на) SITE
     */
    val HANGING_ON = new RelationshipType { override def name = "HANGING_ON" }

    /**
     * рекламная конструкция находится у дороги _прямо здесь_
     * (NORMAL_POINT) -- [RIGHT_THERE] -> (SITE)
     */
    val RIGHT_THERE = new RelationshipType { override def name = "RIGHT_THERE" }

    implicit class PropertyContainerExt(p: PropertyContainer) {
        def copyProperties(other: PropertyContainer) =
            p.getPropertyKeys.asScala.foreach { key => other.setProperty(key, p(key)) }

        def apply(name: String) = if (p.hasProperty(name)) p.getProperty(name) else null
    }

    implicit class NodeExt(node: Node) extends GraphGeometry {
        def latitude = node(LATITUDE).asInstanceOf[Double]
        def longitude = node(LONGITUDE).asInstanceOf[Double]

        def latitudeUTM = node(LATITUDE_UTM).asInstanceOf[Double]
        def longitudeUTM = node(LONGITUDE_UTM).asInstanceOf[Double]

        def wgs = Array(longitude, latitude)
        def utm = Array(longitudeUTM, latitudeUTM)

        def setCoordinate(c: Coordinate): Node = setCoordinate(Array(c.getOrdinate(0), c.getOrdinate(1)))

        def setCoordinate(wgs: Array[Double]) = {
            node.setProperty(LONGITUDE, wgs(0))
            node.setProperty(LATITUDE, wgs(1))
            val utm = wgs2utm(wgs)
            node.setProperty(LONGITUDE_UTM, utm(0))
            node.setProperty(LATITUDE_UTM, utm(1))
            node.setProperty(LONGITUDE_UTM_R, utm(0).round)
            node.setProperty(LATITUDE_UTM_R, utm(1).round)
            node
        }

        def setCoordinateUTM(utm: Array[Double]) = {
            node.setProperty(LONGITUDE_UTM, utm(0))
            node.setProperty(LATITUDE_UTM, utm(1))
            node.setProperty(LONGITUDE_UTM_R, utm(0).round)
            node.setProperty(LATITUDE_UTM_R, utm(1).round)
            val wgs = utm2wgs(utm)
            node.setProperty(LONGITUDE, wgs(0))
            node.setProperty(LATITUDE, wgs(1))
            node
        }

        def relationships(t: RelationshipType) = node.getRelationships(t).asScala
        def relationships(d: Direction, t: RelationshipType) = node.getRelationships(d, t).asScala
        def labels = node.getLabels.iterator.asScala

        def label(name: String) = labels.find(_.name == name)
        def label(l: Label) = labels.find(_ == l)

        def isLabeled(l: Label) = labels.find(_ == l).isDefined

        def distanceTo(other: Node) = distance(node.utm, other.utm)

        def normalTo(road: Relationship) = normal(road.getStartNode.utm, road.getEndNode.utm, node.utm)

        def findClosePoints(deviation: Double, maxDeviation: Double)(implicit db: GraphDatabaseService): Iterator[Node] = findClosePoints(longitudeUTM, latitudeUTM, deviation, maxDeviation)
    }

    implicit class RelationshipExt(r: Relationship) {
        def copyProperties(other: Relationship, skip: Set[String] = Set()) =
            r.getPropertyKeys.asScala.foreach( key =>
                if (!skip.contains(key))
                    other.setProperty(key, r(key))
            )

        def isInsideRoad(x: Array[Double]): Boolean = isInside(r.getStartNode.utm, r.getEndNode.utm, x)

        def setRoad(road: Relationship) = r.setProperty("ROAD_ID", road.getId)

        def road(implicit db: GraphDatabaseService) = db.getRelationshipById(roadID)
        def roadID = r("ROAD_ID").asInstanceOf[Long]
    }

    implicit class PathExt(p: Path) {
        def all = p.nodes.asScala
        def allRelationships = p.relationships.asScala
        def lastRelationshipOption = if (p.lastRelationship != null) Some(p.lastRelationship) else None
        def distance = p.allRelationships.filter(_.isType(GEO_ROAD)).
            foldLeft(0d)((acc, geoRoad) => acc + geoRoad("FEATLEN").asInstanceOf[Double])

        def gids: Option[Set[String]] =
            if (p.endNode.isLabeled(SITE)) {
                val billboards = p.endNode.relationships(HANGING_ON)
                Some(billboards.foldLeft(Set[String]()) { (newGIDs, hangingOn) =>
                    assert(hangingOn.isType(HANGING_ON))
                    assert(hangingOn.getStartNode()("gid") != null)
                    newGIDs + hangingOn.getStartNode()("gid").asInstanceOf[String]
                })
            } else None

        def weight(implicit db: GraphDatabaseService) = {
            def pathRoadsWeights = p.allRelationships.filter(_.isType(GEO_ROAD)).
                foldLeft(Map[Long, Relationship]()) { case (roads, geoRoad) =>
                    if (!roads.contains(geoRoad.roadID)) {
                        roads + (geoRoad.roadID -> geoRoad.road)
                    } else roads
                }

            pathRoadsWeights.map { case (id, road) => road("NLEVEL").asInstanceOf[Int] }.sum
        }
    }

    implicit class TraversalDescriptionExt(t: TraversalDescription) {
        def searchFrom(n: Node) = t.traverse(n).iterator.asScala
    }

    final class CloseAfterTx[A <: Transaction](val x: A) {
        def closeAfter[B](block: A => B): B = {
            try {
                block(x)
            } catch {
                case e: Exception => e.printStackTrace; throw e
            } finally {
                x.success
                x.close
            }
        }
    }

    implicit def any2CloseAfter(x: Transaction): CloseAfterTx[Transaction] = new CloseAfterTx(x)

    final class LockOn(val tx: Transaction) {
        def lock[B, R](res: PropertyContainer)(block: => R): R = {
            val lock = tx.acquireWriteLock(res)
            try {
                block
            } finally {
                lock.release
            }
        }
    }

    implicit def tx2lock(tx: Transaction): LockOn = new LockOn(tx)
}

trait GraphGeometry {
    val NEAREST_POINTS_QUERY =
        "MATCH (p:`GEO_POINT`) " +
            "WHERE p.latitude_utm_r in range({from_latitude}, {to_latitude}) AND " +
            "      p.longitude_utm_r in range({from_longitude},  {to_longitude}) " +
            "RETURN p"

    def findClosePoints(longitudeUTM: Double, latitudeUTM: Double, deviation: Double, step: Double, maxDeviation: Double)
                       (implicit db: GraphDatabaseService): scala.Iterator[Node] = {
/*        println("longitudeUTM = " + longitudeUTM)
        println("latitudeUTM = " + latitudeUTM)
        println("deviation = " + deviation)
        println("maxdeviation = " + maxDeviation)*/
        val res = new ExecutionEngine(db).execute(NEAREST_POINTS_QUERY,
            HashMap(
                "from_latitude" -> (latitudeUTM - deviation).round,
                "to_latitude" -> (latitudeUTM + deviation).round,
                "from_longitude" -> (longitudeUTM - deviation).round,
                "to_longitude" -> (longitudeUTM + deviation).round
            )
        )

        if (res.isEmpty)
            if (deviation <= maxDeviation)
                findClosePoints(longitudeUTM, latitudeUTM, deviation + step, step, maxDeviation)
            else Iterator.empty
        else
            res.map({_("p").asInstanceOf[Node] })
    }

    def findClosePoints(longitudeUTM: Double, latitudeUTM: Double, deviation: Double, maxDeviation: Double)
                       (implicit db: GraphDatabaseService): scala.Iterator[Node] =
        findClosePoints(longitudeUTM, latitudeUTM, deviation, deviation, maxDeviation)
}
