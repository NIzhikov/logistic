package ru.dragon.logistic

import com.typesafe.scalalogging.slf4j.LazyLogging
import org.neo4j.graphdb.Direction._
import org.neo4j.graphdb._
import org.neo4j.graphdb.traversal.BranchOrderingPolicies._
import org.neo4j.graphdb.traversal.Evaluation._
import org.neo4j.graphdb.traversal.Uniqueness._
import org.neo4j.graphdb.traversal._
import ru.dragon.logistic.helpers.Stats

import scala.collection.JavaConverters._
import scala.collection._
import scala.concurrent._
import scala.concurrent.duration.Duration.Inf
import scala.util.{Failure, Success}
import ExecutionContext.Implicits.global

/**
 * @author NIzhikov
 */
trait PathSearcher extends GraphObjects with GraphGeometry with LazyLogging {
    val START_DEVIATION = 80d
    val MAX_DEVIATION = 200d
    val STEP = 50d
    var counters: Stats = Stats("path_searcher")

    def findBest(predicate: Node => Boolean)(coordinate: (Double, Double), maxDistance: Double)(implicit db: GraphDatabaseService) = {
        val all = find(predicate)(coordinate, maxDistance).toList

        val visitedSites = mutable.Set[Int]()
        all.sortBy(p => (p.weight, p.distance)).filter { p =>
            val siteID = p.endNode()("siteID").asInstanceOf[Int]
            if (!visitedSites(siteID)) {
                visitedSites += siteID
                true
            } else false
        }
    }

    def find(predicate: Node => Boolean)(coordinate: (Double, Double), maxDistance: Double)(implicit db: GraphDatabaseService) = counters.time("find") {
        val utm = wgs2utm(Array(coordinate._1, coordinate._2))

        implicit val _predicate = predicate
        implicit val _maxDistance = maxDistance

        counters.time("search") {
            val startPoints = counters.time("start_points_search") { findClosePoints(utm(0), utm(1), START_DEVIATION, STEP, MAX_DEVIATION).toSet }
            val futures = Future.sequence(startPoints.map { point => future(findFromPoint(point, startPoints - point)) })
            Await.ready(futures, Inf)
            futures.value match {
                case None => Iterator.empty
                case Some(x) => x match {
                    case Success (r) => r.flatten
                    case Failure (e) => throw e
                }
            }

                //startPoints.map { point => findFromPoint(point, startPoints - point) }.flatten
        }
    }

    def findFromPoint(point: Node, other: Set[Node])(implicit db: GraphDatabaseService, predicate: Node => Boolean, maxDistance: Double) = counters.time("find_from_point") {
        db.beginTx.closeAfter { case tx =>
            db.traversalDescription.order(PREORDER_BREADTH_FIRST).uniqueness(NODE_PATH).
                expand(new MainRoadExpander(maxDistance, other, counters), new MainRoadInitialBranchState).
                evaluator(new Evaluator {
                override def evaluate(path: Path): Evaluation = counters.time("evaluate_count") {
                    if (path.length == 0)
                        EXCLUDE_AND_CONTINUE
                    else if (!path.endNode.isLabeled(SITE))
                        EXCLUDE_AND_CONTINUE
                    else {
                        val node = path.endNode.relationships(HANGING_ON).head.getStartNode
                        node.label(BILLBOARD) match {
                            case None => EXCLUDE_AND_CONTINUE
                            case Some(_) => if (predicate(node)) INCLUDE_AND_PRUNE else EXCLUDE_AND_CONTINUE
                        }
                    }
                }
            }).searchFrom(point)
        }
    }

    def constructionTypePredicate(t: String): (Node => Boolean) = (n: Node) =>
        n("gid").toString.substring(2, 4) == t
}

sealed case class PathState(distance: Double, roadID: Long, forbiddenRoadsIDs: Array[Long])

sealed class MainRoadInitialBranchState extends InitialBranchState[PathState] {
    override def initialState(path: Path): PathState = PathState(0, -1, Array.empty)
    override def reverse(): InitialBranchState[PathState] = new MainRoadInitialBranchState
}

sealed class MainRoadExpander(maxDistance: Double, other: Set[Node], counters: Stats)(implicit db: GraphDatabaseService) extends PathExpander[PathState] with GraphObjects {
    def ROAD_TYPES = Set("N" /*Дорога*/ , "I" /*Дорога в складских зонах*/ , "P" /*Дорога до POI*/ , "R" /*Дорога внутри парковки*/)

    override def reverse(): PathExpander[PathState] = new MainRoadExpander(maxDistance, other, counters)

    override def expand(path: Path, branchState: BranchState[PathState]): java.lang.Iterable[Relationship] = counters.time("expand") {
        if (counters.count("expand") % 10000 == 0)
            counters.printAll

        val end = path.endNode
        if (end.hasLabel(NORMAL_POINT))
            end.relationships(OUTGOING, RIGHT_THERE)
        else {
            val state = newState(path, branchState)
            if (state.distance > maxDistance)
                List.empty
            else
                end.relationships(OUTGOING, TO_NORMAL) ++
                end.relationships(OUTGOING, GEO_ROAD)
                    .filter{ r => !other(r.getStartNode) && !other(r.getEndNode)}
                    .filter { r =>
                        val road = r.road
                        !state.forbiddenRoadsIDs.contains(r.roadID) &&
                        road("NLEVEL").asInstanceOf[Int] < 7 && /*Внутриквартальные проезды и подьезды*/
                            ROAD_TYPES(road("LTYPE").asInstanceOf[String])
                    }.toList.sortBy { r =>
                        val road = r.road
                        -road("NLEVEL").asInstanceOf[Int]
                    }
        }
    }.asJava

    def newState(path: Path, branchState: BranchState[PathState]) = counters.time("new_state") {
        val state = branchState.getState
        path.lastRelationshipOption match {
            case Some(lastRelationship) =>
                if (lastRelationship.isType(GEO_ROAD)) {
                    val newDistance = state.distance + lastRelationship("FEATLEN").asInstanceOf[Double]
                    val newState =
                        if (state.roadID != lastRelationship.roadID) {
                            val newRoad = lastRelationship.road
                            val propertyName =
                                if (path.endNode == newRoad.getStartNode) "forbiddenRoadsFromEnd"
                                else "forbiddenRoadsFromStart"
                            val forbiddenRoads = Option(newRoad(propertyName).asInstanceOf[Array[Long]])
                            state.copy(distance = newDistance, roadID = lastRelationship.roadID,
                                forbiddenRoadsIDs = forbiddenRoads.getOrElse(Array.empty))
                        } else state.copy(distance = newDistance)
                    branchState.setState(newState)
                    newState
                } else
                    state
            case None => state
        }
    }
}
