package ru.dragon.logistic.preparer

import org.neo4j.graphdb.{Node, Direction, Relationship, GraphDatabaseService}
import org.neo4j.graphdb.Direction._
import ru.dragon.logistic.GraphObjects
import ru.dragon.logistic.helpers.{Stats, GraphWorker}

import scala.annotation.tailrec

/**
 * @author NIzhikov
 */
object DivideRoad extends GraphWorker with GraphObjects {
    val counters = Stats("divide_road")

    /**
     * Разбивает длиные ребра графа на участки по 50 или менее метров
     */
    def run()(implicit db: GraphDatabaseService) = counters.time("full_time") {
        db.beginTx.closeAfter { tx =>
            val smallRoads = execute("MATCH x-[r:`GEO_ROAD`]->y " +
                "WHERE (x.latitude_utm-y.latitude_utm)*(x.latitude_utm-y.latitude_utm) + (x.longitude_utm-y.longitude_utm)*(x.longitude_utm-y.longitude_utm) > 100*100 " +
                "RETURN r").map(r => r("r").asInstanceOf[Relationship])

            smallRoads.foldLeft(Map[(Long, Long), Long]()) { case (existed, r) => counters.time("count") {
                if (!existed.contains(r.getEndNode.getId -> r.getStartNode.getId)) {
                    //Такой дороги мы еще не обрабатывали. Просто разделяем ее на мелкие отрезки
                    val lastRoad = divideForward(r)
                    r.delete
                    existed + ((r.getStartNode.getId -> r.getEndNode.getId) -> lastRoad.getId)
                } else {
                    //Уже обрабатывали эту дорогу в "прямом" направлении. Проходим в "обратном"
                    divideReverse(r.getEndNode.getId, existed(r.getEndNode.getId -> r.getStartNode.getId))
                    r.delete
                    existed
                }
            }}
        }
    }

    def divideForward(r: Relationship)(implicit db: GraphDatabaseService) = {
        counters.incr("forward")

        val utmPoints = divideRoad(50) (r.getStartNode.utm, r.getEndNode.utm)
        val lastPoint = utmPoints.foldLeft(r.getStartNode) { case (node, utmPoint) =>
            val newPoint = db.createNode(DIVIDE_POINT)
            newPoint.addLabel(GEO_POINT)
            node.copyProperties(newPoint)

            val newRoad = node createRelationshipTo(newPoint, GEO_ROAD)
            r.copyProperties(newRoad)

            newPoint.setCoordinateUTM(utmPoint)
        }

        val lastRoad = lastPoint createRelationshipTo(r.getEndNode, GEO_ROAD)
        r.copyProperties(lastRoad)
        lastRoad
    }

    def divideReverse(stopPointID: Long, startRelationshipID: Long)(implicit db: GraphDatabaseService) = {
        counters.incr("reverse")

        def traverseReverse(stopNodeID: Long, r: Relationship): Unit = {
            if (r.getStartNode.getId != stopNodeID) {
                val geoRoads = r.getStartNode.relationships(INCOMING, GEO_ROAD)
                val count = geoRoads.foldLeft(0) { case (count, geoRoad) =>
                    if (count != 0)
                        throw new IllegalStateException("More then 1 incoming GEO_ROAD relationships. NodeID = " + r.getStartNode.getId)

                    val reverseGeoRoad = r.getEndNode.createRelationshipTo(r.getStartNode, GEO_ROAD)
                    r.copyProperties(reverseGeoRoad)

                    traverseReverse(stopNodeID, geoRoad)

                    1
                }

                if (count == 0)
                    throw new IllegalStateException("There is no incoming GEO_ROAD relationships. NodeID = " + r.getStartNode.getId)

            } else {
                val reverseGeoRoad = r.getEndNode.createRelationshipTo(r.getStartNode, GEO_ROAD)
                r.copyProperties(reverseGeoRoad)
            }
        }

        traverseReverse(stopPointID, db.getRelationshipById(startRelationshipID))
    }
}
