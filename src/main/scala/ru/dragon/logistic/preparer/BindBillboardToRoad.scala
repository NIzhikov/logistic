package ru.dragon.logistic.importer

import com.typesafe.scalalogging.slf4j.LazyLogging
import org.neo4j.graphdb._
import org.neo4j.kernel.DeadlockDetectedException
import ru.dragon.logistic.GraphObjects
import ru.dragon.logistic.helpers.{Normal, GraphWorker, Stats}

import scala.annotation.tailrec
import scala.collection._
import scala.concurrent._
import scala.concurrent.duration.Duration.Inf
import ExecutionContext.Implicits.global

/**
 * @author dragon
 */
object BindBillboardToRoad extends GraphWorker with GraphObjects with LazyLogging {
    val counters = Stats("bind_billboard")
    val START_DEVIATION = 50d
    val MAX_DEVIATION = 400d
    val MAX_NORMAL_LENGTH = 75d

    val COUNT_NOT_CONNECTED = "MATCH (s:`SITE`) WHERE s.IS_CONNECTED IS NULL RETURN COUNT(s)"
    val SITES_QUERY = "MATCH (s:`SITE`) WHERE s.IS_CONNECTED IS NULL RETURN s"

    override def run()(implicit db: GraphDatabaseService): Unit =  counters.time("full") {
        val sitesFutures = db.beginTx.closeAfter { tx =>
            counters.value("not_connected") { execute(COUNT_NOT_CONNECTED).next()("COUNT(s)").asInstanceOf[Long] }
            if (counters.get("not_connected") == 0)
                return;

            counters.print("not_connected")
            Future.sequence(execute(SITES_QUERY).map(r => r("s").asInstanceOf[Node]).map {
                site => future(bind2road(site)(db))
            })
        }
        Await.ready(sitesFutures, Inf)
    }

    def bind2road(site: Node)(implicit db: GraphDatabaseService) = counters.time("bind") {
        def try2bind(tryCount: Int): Unit = {
            try {
                doBind2road(site)
            } catch {
                case e: DeadlockDetectedException => {
                    logger.error("DeadLock", e)
                    if (tryCount > 0)
                        try2bind(tryCount-1)
                    else
                        throw e
                }
            }
        }
        try2bind(3)
    }

    def doBind2road(site: Node)(implicit db: GraphDatabaseService) = db.beginTx.closeAfter { tx =>
        //Алгоритм привязки:
        //1. Для каждой точки близкой к конструкции
        //2. Взять все с ней связанные точки
        //3. Вычислить расстояние от конструкции до дороги
        //4. Построить перпендикуляр
        //5. Запомнить перпендикуляры, которые падают между точками(косоугольный треугольник)
        //6. Привязать конструкцию к перпендикуляру,
        //7. Привязать точки дороги к перпендикуляру, удалить старую связь
        findCloseRoads(site, START_DEVIATION) match {
            case Nil => { counters.incr("normal_not_found"); logger.warn("Site without normals. ID = " + site.getId + ", c = " + site.wgs.mkString("(", ", ", ")")) }
            case normals: List[(Relationship, Normal)] => {
                normals.foreach { case (geoRoad, normal) =>
                    counters.incr("normal_count")
                    val normalNode = db.createNode(NORMAL_POINT)
                    normalNode.setCoordinateUTM(normal.coordinateUTM)
                    normalNode createRelationshipTo(site, RIGHT_THERE)

                    tx.lock(geoRoad.getStartNode) {
                        geoRoad.getStartNode createRelationshipTo(normalNode, TO_NORMAL)
                    }
                }
                site.setProperty("IS_CONNECTED", true)
            }
        }
        counters.print("bind")
    }

    @tailrec
    def findCloseRoads(node: Node, deviation: Double)(implicit db: GraphDatabaseService): List[(Relationship, Normal)] = {
        val visitedRoads = mutable.Set[Long]()
        val roads = node.findClosePoints(deviation, MAX_DEVIATION).flatMap { point =>
            val geoRoads =  point.relationships(GEO_ROAD)

            geoRoads.map { road => (road, node normalTo road) }.filter { case (road, normal) =>
                road.isInsideRoad(normal.coordinateUTM) && normal.length <= MAX_NORMAL_LENGTH
            }
        }.toList.sortBy(_._2).filter { case (r, n) =>
            if (!visitedRoads(r.roadID)) {
                visitedRoads += r.roadID
                true
            } else false
        }

        if (roads.nonEmpty && roads.size > 1)
            roads
        else if (deviation < MAX_DEVIATION)
            findCloseRoads(node, deviation * 2)
        else
            roads
    }
}
