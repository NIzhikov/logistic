package ru.dragon.logistic.preparer

import org.neo4j.graphdb.Direction._
import org.neo4j.graphdb.{Relationship, GraphDatabaseService}
import org.opengis.feature.`type`.AttributeDescriptor
import org.opengis.feature.simple.SimpleFeature
import ru.dragon.logistic.GraphObjects
import ru.dragon.logistic.helpers.{Using, ShapeFileHelper, Stats, GraphWorker}
import scala.concurrent._
import scala.concurrent.duration.Duration.Inf
import ExecutionContext.Implicits.global

import scala.concurrent.Future

/**
 * @author NIzhikov
 */
object FixForbiddenTurns extends GraphWorker with GraphObjects with ShapeFileHelper with Using {
    val counters = Stats("fix_forbidden_turn")

    def run()(implicit db: GraphDatabaseService) = {
        val roads = shapeIterator("file://" + ROOT + "/ArcView/no_turn.shp")
        implicit val attributes = roads.attributes

        val futures = Future.sequence(roads.map(r => future(fixTurn(r))))
        Await.ready(futures, Inf)
        counters.printAll
    }

    def fixTurn(turn: SimpleFeature)(implicit db: GraphDatabaseService, ad: Iterable[AttributeDescriptor]): Unit =
        db.beginTx.closeAfter { tx =>
            counters.time("fix_turn") {
                if (!checkTurn(turn))
                    return

                road(turn("EDGE1ID").asInstanceOf[Int]) match {
                    case None => {
                        counters.incr("edge1_not_found"); counters.value("edge1_not_found") {
                            turn("EDGE1ID")
                        }
                    }
                    case Some(start) => {
                        val way = List(
                            road(turn("EDGE2ID").asInstanceOf[Int]),
                            road(turn("EDGE3ID").asInstanceOf[Int]),
                            road(turn("EDGE4ID").asInstanceOf[Int]),
                            road(turn("EDGE5ID").asInstanceOf[Int])).flatten

                        //Здесь "неправильный"(обратный) старт поиска,
                        //При первом проходе foldLeft нужно пойти "правильную" вершину
                        val startNode = turn("EDGE1END") match {
                            case "Y" => start.getEndNode
                            case "N" => start.getStartNode
                        }

                        way.foldLeft((startNode, start)) { case ((startNode, current), next) =>
                            startNode.relationships(ROAD).filter(_ == next).toList match {
                                case next :: Nil => {
                                    counters.incr("edges_processed")

                                    val propName =
                                        if (next.getStartNode == startNode) "forbiddenRoadsFromStart" else "forbiddenRoadsFromEnd"
                                    val forbiddenTurns = Option(current(propName).asInstanceOf[Array[Long]]).getOrElse(Array[Long]())
                                    if (!forbiddenTurns.contains(next.getId))
                                        current.setProperty(propName, forbiddenTurns :+ next.getId)

                                    if (next.getStartNode == startNode)
                                        (next.getEndNode, next)
                                    else
                                        (next.getStartNode, next)
                                }
                                case Nil => throw new RuntimeException("not found!")
                                case _ => throw new RuntimeException("more then one found!")
                            }
                        }

                    }
                }
            }
            counters.print("fix_turn")
        }

    def checkTurn(turn: SimpleFeature): Boolean = {
        /*Распространяется ли запрет на автомобили?*/
        if (turn("R_AUTO") != "Y") {
            counters.incr("skip_not4car")
            return false
        }

        /*Дни, когда запрет дейтсвует. N - всегда. W - в рабочие дни, H - выходные, E - во все дни*/
        if (!Set("N", "W", "E")(turn("TURNDAYS").toString)) {
            counters.incr("skip_not_always")
            return false
        }

        /*Запрет по правилам ПДД. Другое значение L - запрет "исходя из здравого смысла" */
        if (turn("TURNTYPE") != "R") {
            counters.incr("skip_turntype")
            return false
        }

        true
    }

    def road(id: Int)(implicit db: GraphDatabaseService): Option[Relationship] =
        if (id == 0)
            None
        else {
            val res = execute("MATCH (a:`POINT`)-[r:`ROAD`]-b WHERE r.ID = {id} RETURN r LIMIT 1", Map("id" -> id)).map(r => r("r").asInstanceOf[Relationship])
            if (!res.isEmpty)
                Some(res.next)
            else
                None
        }
}