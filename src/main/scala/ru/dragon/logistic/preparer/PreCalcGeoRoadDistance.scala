package ru.dragon.logistic.preparer

import org.neo4j.graphdb.{Relationship, GraphDatabaseService}
import ru.dragon.logistic.GraphObjects
import ru.dragon.logistic.helpers.{Stats, GraphWorker}

/**
 * @author NIzhikov
 */
object PreCalcGeoRoadDistance extends GraphWorker with GraphObjects {
    val counters: Stats = Stats("pre_calc_geo_road_distance")
    
    override def run()(implicit db: GraphDatabaseService) = counters.time("full") {
        db.beginTx.closeAfter { tx =>
            execute("MATCH x-[r:`GEO_ROAD`]->y RETURN r").
                map(r => r("r").asInstanceOf[Relationship]).
                foreach { road => counters.time("count") {
                    val distance = road.getStartNode distanceTo road.getEndNode
                    road.setProperty("FEATLEN", distance)
                }}
        }
    }
}
