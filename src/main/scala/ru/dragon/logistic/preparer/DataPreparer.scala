package ru.dragon.logistic.preparer

import ru.dragon.logistic.GraphDB
import ru.dragon.logistic.importer.BindBillboardToRoad

/**
 * @author dragon
 */

object DataPreparer extends App {
    implicit val db = GraphDB()

    if (args.isEmpty) {
        println("options: -prepareSmallRoads -prepareGeoRoads -bindBillboards -fixForbiddenTurns")
    } else {
        if (args.contains("-prepareSmallRoads")) {
            DivideRoad.run
            DivideRoad.counters.printAll
        }

        if (args.contains("-prepareGeoRoads")) {
            PreCalcGeoRoadDistance.run
            PreCalcGeoRoadDistance.counters.printAll
        }

        if (args.contains("-bindBillboards")) {
            BindBillboardToRoad.run
            BindBillboardToRoad.counters.printAll
        }

        if (args.contains("-fixForbiddenTurns")) {
            FixForbiddenTurns.run
            FixForbiddenTurns.counters.printAll
        }
    }
}
