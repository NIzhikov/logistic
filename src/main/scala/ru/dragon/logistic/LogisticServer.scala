package ru.dragon.logistic

import com.twitter.finatra.{Controller, FinatraServer, View}
import org.neo4j.graphdb.Node
import ru.dragon.logistic.helpers.{Using, GraphDBController}
import scala.collection._

object LogisticServer extends FinatraServer {
    register(new FindPathesController)
}

class FindPathesController extends Controller with GraphDBController with PathSearcher with Using {
    implicit val db = GraphDB()

    get("/show-billboards.html") { request =>
        render.view(new View {
            val template = "templates/show_billboards.mustache"
        }).toFuture
    }

    post("/find-routes.json") { request =>
        val bbPredicate = constructionTypePredicate("BB")
        val ssPredicate = constructionTypePredicate("SS")

        val longitude = request.getParam("lon").toDouble
        val latitude = request.getParam("lat").toDouble
        val maxDistance = request.getParam("distance").toInt
        renderWithGraphDB.json(Map(
            "center" -> Array(longitude, latitude),
            "routes" -> db.beginTx.closeAfter { tx =>
                findBest { n => bbPredicate(n) || ssPredicate(n)}((longitude, latitude), maxDistance).map { p => Map(
                    "length" -> p.distance,
                    "gids" -> (p.gids match {
                        case Some(gids) => gids
                        case None => Set.empty
                    }),
                    "route" -> p.all.map { n =>
                        if (n.isLabeled(GEO_POINT) || n.isLabeled(NORMAL_POINT)) {
                            Array[AnyVal](
                                n(LATITUDE).asInstanceOf[Double],
                                n(LONGITUDE).asInstanceOf[Double])
                        } else {
                            Array[AnyVal](
                                n(LATITUDE).asInstanceOf[Double],
                                n(LONGITUDE).asInstanceOf[Double],
                                n("siteID").asInstanceOf[Integer],
                                n.getId)
                        }
                    })
                }
            })).toFuture.onSuccess { a => counters.printAll }
    }

    get("/not-connected.json") { request =>
        db.beginTx.closeAfter { tx =>
            renderWithGraphDB.json(execute("MATCH (s:`SITE`) WHERE s.IS_CONNECTED is null RETURN s").
                map { s=> s("s").asInstanceOf[Node]}.
                map { s => Array(s.latitude, s.longitude) }
            ).toFuture
        }
    }
}