package ru.dragon.logistic.helpers

import java.text.DecimalFormat

import com.typesafe.scalalogging.slf4j.LazyLogging

import scala.collection.mutable

class Stats(name: String) extends LazyLogging with PrettyPrinter {
    val counters = new mutable.HashMap[String, Counter]

    private def create(name: String) = this.synchronized {
        if (counters.contains(name))
            counters(name)
        else {
            counters(name) = new Counter
            counters(name)
        }
    }

    def getOrCreate(name: String) =
        if (!counters.contains(name))
            create(name)
        else
            counters(name)

    def time[R](name: String)(calculation: => R) = {
        val now = System.currentTimeMillis
        val c = getOrCreate(name)
        c.incr
        val r = calculation
        c.addTime(System.currentTimeMillis - now)
        r
    }

    def incr(name: String) = getOrCreate(name).incr

    def count(name: String) = getOrCreate(name).count

    def get(name: String) = getOrCreate(name).value

    def value[R](name: String)(producer: => R) =
        getOrCreate(name).setValue(producer)

    def printAll = counters.keySet.foreach(print)

    def print(n: String) = {
        val counter = getOrCreate(n).update
        val r = List(
            if (counter.count != 0) ("count = " + format(counter.count)) else "",
            if (counter.average != 0) "average = " + format(counter.average) else "",
            if (counter.last != 0) "last = " + format(counter.last) else "",
            if (counter.time != 0) "time = " + formatDuration(counter.time) else "",
            if (counter.value != null) "value = " + counter.value.toString else "").filter(_!="")

        logger.info(name + "." + n + ": " + r.mkString(", "))
    }
}

object Stats {
    def apply(name: String) = new Stats(name)
}

case class Counter(var count: Long = 0l, var time: Long = 0l, var average: Double = 0l, var last: Double = 0l, var value: Any = null) {
    def update = this.synchronized {
        if (count != 0)
            average = time.toDouble/count
        this
    }

    def incr = this.synchronized { count += 1; count }

    def addTime(t: Long) = this.synchronized { time += t; last = t }

    def setValue(v: Any) = this.synchronized { value = v }
}
