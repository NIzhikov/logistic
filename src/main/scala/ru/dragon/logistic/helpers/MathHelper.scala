package ru.dragon.logistic.helpers

/**
 * @author NIzhikov
 */
trait MathHelper {
    def round(x: Double, n: Int) = Math.round(x*Math.pow(10, n))/Math.pow(10, n).toDouble
}
