package ru.dragon.logistic.helpers

import com.twitter.finatra.{Request, ResponseBuilder}

import com.twitter.finagle.http.Response
import ru.dragon.logistic.{GraphObjects, GraphDB}

/**
 * @author NIzhikov
 */

trait GraphDBController extends GraphObjects {
    def renderWithGraphDB = new ResponseBuilder {
        val db = GraphDB()

        override def build(request: Request): Response =  db.beginTx.closeAfter { tx =>
            super.build(request)
        }
    }
}
