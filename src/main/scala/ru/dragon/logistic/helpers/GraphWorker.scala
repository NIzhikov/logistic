package ru.dragon.logistic.helpers

import org.neo4j.graphdb.GraphDatabaseService

/**
 * @author NIzhikov
 */
trait GraphWorker {
    def run()(implicit db: GraphDatabaseService)
    def counters: Stats

}
