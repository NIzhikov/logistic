package ru.dragon.logistic.helpers

import java.text.DecimalFormat

/**
 * @author NIzhikov
 */
trait PrettyPrinter {
    private val f1 = new DecimalFormat("###,##0")
    private val f2 = new DecimalFormat("###,##0.000")

    def format(v: Int): String = f1.format(v)
    def format(v: Long): String = f1.format(v)
    def format(v: Double): String = f2.format(v)
    def format(v: Float): String = f2.format(v)

    def formatDuration(v: Long) = {
        val h = v/3600000
        val M = (v - h*3600000) / 60000
        val s = (v - h*3600000 - M*60000) / 1000
        val m = (v - h*3600000 - M*60000) % 1000

        "%dH:%02dM:%02ds.%03dm" format (h, M, s, m)
    }
}
