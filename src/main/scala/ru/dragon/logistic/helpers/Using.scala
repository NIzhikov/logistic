package ru.dragon.logistic.helpers

import org.neo4j.graphdb.Transaction

/**
 * @author NIzhikov
 */
trait Using {
    type Closeable = {def close():Unit }

    final class CloseAfter[A <: Product](val x: A) {
        def closeAfter[B](block: A => B): B = {
            try {
                block(x)
            } finally {
                for (i <- 0 until x.productArity)
                    x.productElement(i) match {
                        case c: Transaction => c.success; c.close
                        case c: Closeable => c.close
                        case _ =>
                    }
            }
        }
    }

    final class CloseAfter2[A <: Closeable](val x: A) {
        def closeAfter[B](block: A => B): B = {
            try {
                block(x)
            } finally {
                x.close
            }
        }
    }

    implicit def any2CloseAfter[A<:Product](x: A): CloseAfter[A] = new CloseAfter(x)

    implicit def closeable2CloseAfter[A<:Closeable](x: A): CloseAfter2[A] = new CloseAfter2(x)
}