package ru.dragon.logistic.helpers

/**
 * @author NIzhikov
 */
trait UntypedPropertyWrapper {
    case class PropertyWrapper(v: Any)

    implicit def pw2Int(pw: PropertyWrapper): Int = pw.v.asInstanceOf[Int]
    implicit def pw2Long(pw: PropertyWrapper): Long = pw.v.asInstanceOf[Long]
    implicit def pw2String(pw: PropertyWrapper): String = pw.v.asInstanceOf[String]
    implicit def pw2Double(pw: PropertyWrapper): Double = pw.v.asInstanceOf[Double]
}
