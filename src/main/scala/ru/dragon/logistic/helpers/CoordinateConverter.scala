package ru.dragon.logistic.helpers

import org.geotools.referencing.{CRS, ReferencingFactoryFinder}
import org.geotools.referencing.factory.ReferencingFactoryContainer
import org.geotools.referencing.crs.DefaultGeographicCRS
import org.geotools.referencing.cs.DefaultCartesianCS
import java.util.Collections

/**
 * @author dragon
 */
trait CoordinateConverter extends MathHelper {
    private val UTM_ZONE_CENTER_LATITUDE = 39.0d
    private val ZONE_NUMBER = 37

    private lazy val _utm2wgs = {
        val mtFactory = ReferencingFactoryFinder.getMathTransformFactory(null)
        val factories = new ReferencingFactoryContainer(null)
        val geoCRS = DefaultGeographicCRS.WGS84
        val cartCS = DefaultCartesianCS.GENERIC_2D
        val parameters = mtFactory.getDefaultParameters("Transverse_Mercator")
        parameters.parameter("central_meridian").setValue(UTM_ZONE_CENTER_LATITUDE)
        parameters.parameter("latitude_of_origin").setValue(0.0)
        parameters.parameter("scale_factor").setValue(0.9996)
        parameters.parameter("false_easting").setValue(500000.0)
        parameters.parameter("false_northing").setValue(0.0)

        val properties = Collections.singletonMap("name", "WGS 84 / UTM Zone " + ZONE_NUMBER)
        val projCRS = factories.createProjectedCRS(properties, geoCRS, null, parameters, cartCS)

        CRS.findMathTransform(projCRS, geoCRS)
    }
    private lazy val wgs2utm = {
        val mtFactory = ReferencingFactoryFinder.getMathTransformFactory(null)
        val factories = new ReferencingFactoryContainer(null)
        val geoCRS = DefaultGeographicCRS.WGS84
        val cartCS = DefaultCartesianCS.GENERIC_2D
        val parameters = mtFactory.getDefaultParameters("Transverse_Mercator")
        parameters.parameter("central_meridian").setValue(UTM_ZONE_CENTER_LATITUDE)
        parameters.parameter("latitude_of_origin").setValue(0.0)
        parameters.parameter("scale_factor").setValue(0.9996)
        parameters.parameter("false_easting").setValue(500000.0)
        parameters.parameter("false_northing").setValue(0.0)

        val properties = Collections.singletonMap("name", "WGS 84 / UTM Zone " + ZONE_NUMBER)
        val projCRS = factories.createProjectedCRS(properties, geoCRS, null, parameters, cartCS)

        CRS.findMathTransform(geoCRS, projCRS)
    }

    def wgs2utm(in: Array[Double]): Array[Double] = {
        val out = Array(.0d, .0d)
        wgs2utm.transform(in, 0, out, 0, 1)
        Array(round(out(0), 3), round(out(1), 3))
    }

    def utm2wgs(in: Array[Double]): Array[Double] = {
        val out = Array(.0d, .0d)
        _utm2wgs.transform(in, 0, out, 0, 1)
        Array(round(out(0), 6), round(out(1), 6))
    }

}
