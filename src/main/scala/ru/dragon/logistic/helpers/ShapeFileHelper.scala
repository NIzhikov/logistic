package ru.dragon.logistic.helpers

import java.io.Closeable
import java.net.URL
import java.nio.charset.Charset

import com.vividsolutions.jts.geom.MultiLineString
import org.geotools.data.shapefile.ShapefileDataStore
import org.opengis.feature.`type`.AttributeDescriptor
import org.opengis.feature.simple.SimpleFeature
import scala.collection.JavaConverters._
import org.opengis.feature.`type`.Name

/**
 * @author NIzhikov
 */
trait ShapeFileHelper {
    val ROOT = "/home/dragon/src/logistic/maps/files"

    def shapeIterator(url: String) = {
        val store = new ShapefileDataStore(new URL(url))
        store.setCharset(Charset.forName("Cp1251"))
        sys.addShutdownHook(store.dispose)
        val reader = store.getFeatureReader
        new Iterator[SimpleFeature] with Closeable {
            override def hasNext: Boolean = reader.hasNext
            override def next: SimpleFeature = reader.next

            def attributes: Iterable[AttributeDescriptor] = store.getSchema.getAttributeDescriptors.asScala
            def close = reader.close
        }
    }

    implicit def name2string(n: Name) = n.toString

    implicit class SimpleFeatureExt(f: SimpleFeature) {
        def apply(name: String) = f.getAttribute(name)

        def isOneWay = f("ONEWAY") match {
            case "B" => false
            case _ => true
        }
        
        def coordinates = f.getDefaultGeometry.asInstanceOf[MultiLineString].getCoordinates
    }
}
