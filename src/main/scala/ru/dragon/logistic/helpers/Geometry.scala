package ru.dragon.logistic.helpers

import java.lang.Math._

/**
 * @author NIzhikov
 */
trait Geometry extends MathHelper {
    val LATITUDE = "latitude"
    val LONGITUDE = "longitude"
    val LATITUDE_UTM = "latitude_utm"
    val LONGITUDE_UTM = "longitude_utm"
    val LATITUDE_UTM_R = "latitude_utm_r"
    val LONGITUDE_UTM_R = "longitude_utm_r"

    implicit class DoubleExt(v: Double) {
        def toMercator = v * 180 / Math.PI
    }

    implicit class FloatExt(v: Float) {
        def toMercator = v * 180 / Math.PI
    }

    private def length(x: Array[Double], y: Array[Double]) =
        sqrt(pow(x(0)-y(0), 2) + pow(x(1)-y(1), 2))

    def distance(x: Array[Double], y: Array[Double]) = sqrt(pow(x(0) - y(0), 2) + pow(x(1)-y(1), 2))

    def normal(A: Array[Double], B: Array[Double], C: Array[Double]): Normal =  {
        //Длины сторон
        val a = length(A, B)
        val b = length(A, C)
        val c = length(B, C)

/*        println("a = " + a)
        println("b = " + b)
        println("c = " + c)*/

        if (b == 0 || c == 0) {
            Normal(distance(A, C), A)
        } else {
            //Длина полупериметра
            val p = (a + b + c)/2
/*            println("p = " + p)*/
            //Площадь
            val S = sqrt(p*(p-a)*(p-b)*(p-c))
/*            println("S = " + S)*/
            //Длина высоты
            val h = 2*S/a
/*            println("h = " + h)*/
            val sinA = h/b
/*            println("sinA = " + sinA)*/
            //Длина отрезка от A до пересечения с перпендикуляром
            val AX = sqrt((1 - pow(sinA, 2))*pow(b, 2))
/*            println("AX = " + AX)*/
            val isCAB_GT_90 = (a*a + b*b > c*c)

            val X_x = if (isCAB_GT_90)
                A(0) + (AX / a) * (B(0) - A(0))
            else
                A(0) - (AX / a) * (B(0) - A(0))

            val ySIGN = (isCAB_GT_90 && A(1) < B(1)) || (!isCAB_GT_90 && A(1) > B(1))
            val X_y = if (ySIGN)
                A(1) + sqrt(pow(AX, 2) - pow(X_x - A(0), 2))
            else
                A(1) - sqrt(pow(AX, 2) - pow(X_x - A(0), 2))

            Normal(round(h, 3), Array(round(X_x, 3), round(X_y, 3)))
        }
    }

    def fromA2B(l: Double)(A: Array[Double], B: Array[Double]): Array[Double] = {
        val AB = length(A, B)
        val X_x = A(0) + (B(0) - A(0)) * l / AB
        val X_y =  if (A(1) <= B(1))
            A(1) + sqrt(l*l - pow(X_x - A(0), 2))
        else
            A(1) - sqrt(l*l - pow(X_x - A(0), 2))
        Array(round(X_x, 3), round(X_y, 3))
    }

    def divideRoad(divideLength: Double)(A: Array[Double], B: Array[Double]): Array[Array[Double]] = {
        def doDivideRoad(divideLength: Double)(A: Array[Double], B: Array[Double]): Array[Array[Double]] = {
            val l = length(A, B)
            if (l <= divideLength)
                Array.empty
            else {
                val X = fromA2B(divideLength)(A, B)
                Array(X) ++ divideRoad(divideLength)(X, B)
            }
        }

        if (A(0) <= B(0))
            doDivideRoad(divideLength)(A, B)
        else
            doDivideRoad(divideLength)(B, A).reverse
    }

    def isInside(A: Array[Double], B: Array[Double], C: Array[Double]): Boolean = {
        C(0) >= min(A(0), B(0)) && C(0) <= max(A(0), B(0)) &&
            C(1) >= min(A(1), B(1)) && C(1) <= max(A(1), B(1))
    }
}

case class Normal(val length: Double, val coordinateUTM: Array[Double]) extends Ordered[Normal] {
    override def compare(that: Normal): Int = return length.compareTo(that.length)
}

