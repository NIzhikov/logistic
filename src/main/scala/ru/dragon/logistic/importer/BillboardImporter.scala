package ru.dragon.logistic.importer

import org.neo4j.graphdb._
import org.neo4j.graphdb.Label
import ru.dragon.logistic.GraphObjects
import ru.dragon.logistic.helpers.{GraphWorker, Geometry, Stats, CoordinateConverter}
import scala.slick.session.{Session, Database}
import scala.slick.jdbc.{GetResult, StaticQuery}

/**
 * @author NIzhikov
 */
object BillboardImporter extends GraphWorker with GraphObjects {
    val counters = Stats("billboard_importer")

    val QUERY =
        "SELECT " +
            "sa.cod_adres id, " +
            "ISNULL(sp.i_site_id, -1) i_site_id, " +
            "ar.i_id as regionID, " +
            "ar.s_name as regionName, " +
            "sa.i_sub_area_id subRegionID, " +
            "c.f_lat latitude, " +
            "c.f_long longitude, " +
            "c.i_angle angle, " +
            "sa.adres address, " +
            "sa.storona storona, " +
            "sa.nomer, " +
            "sa.i_constr_type_id billboardTypeID, " +
            "dbo.gid(sa.cod_adres, 0) as gid, " +
            "ct.s_id_code as ctCode " +
        "FROM " +
            "spr_adre sa (nolock) LEFT JOIN " +
            "constr_type ct (nolock) ON ct.i_id = sa.i_constr_type_id LEFT JOIN " +
            "site_panels sp (nolock) ON sp.i_panel_id = sa.COD_ADRES JOIN " +
            "panel_intervals pi (nolock) ON pi.i_panel_id = sa.COD_ADRES JOIN " +
            "gg_coord  c (nolock) ON c.i_id = sa.cod_adres LEFT JOIN " +
            "light_link ll (nolock) ON ll.i_panel_id = sa.cod_adres LEFT JOIN " +
            "stat st (nolock) on st.i_id = sa.cod_adres JOIN " +
            "areas ar (nolock) on ar.i_id = sa.i_area_id " +
        "WHERE " +
            "ct.s_id_code IN ('BB', 'CF', 'SS') AND " +
            "pi.i_type = 0 AND " +
            "(pi.d_finish IS NULL OR (pi.d_start <= GETDATE() AND pi.d_finish >= GETDATE())) AND " +
            "ar.i_id IN (2, 4) AND " +
            "(c.f_lat*180/3.141596) BETWEEN 55 AND 56 AND " +
            "(c.f_long*180/3.141596) BETWEEN 37 AND 38 " +
        "ORDER BY ISNULL(sp.i_site_id, -1) DESC, c.f_lat, c.f_long"

    case class Billboard(val id: Int, val siteID: Int, val regionID: Int, val regionName: String, val subRegionID: Int,
                         val latitude: Float, val longitude: Float, val angle: Int,
                         val address: String, val storona: String, val number: Int, val billboardTypeID: Int,
                         val gid: String, val ctCode: String) {
        def siteData = (siteID, latitude.toMercator, longitude.toMercator)

        def propertiesAsPairs = {
            for (field <- this.getClass.getDeclaredFields()) yield {
                field.setAccessible(true)

                if (field.getName == LATITUDE || field.getName == LONGITUDE)
                    (field.getName, field.get(this).asInstanceOf[Float].toMercator)
                else
                    (field.getName, field.get(this))
            }
        }

    }
    implicit val getBillboardResult = GetResult(r => Billboard(r.<<, r <<, r <<, r <<, r <<, r <<, r <<, r <<, r <<, r <<, r <<, r <<, r <<, r <<))

    override def run()(implicit db: GraphDatabaseService) = counters.time("full_time") {
        Database.forURL("jdbc:jtds:sqlserver://SPHINX.aprcity.com/Tech;instance=SQLTEST2005;user=WebServer;password=GeP71$za_F",
            driver = "net.sourceforge.jtds.jdbc.Driver") withSession { implicit session: Session =>
                val fakeID = Stream.from(-1, -1).iterator
                var siteData = (-1, -1d, -1d)
                var site: Node = null

                val billboards = StaticQuery.queryNA[Billboard](QUERY).list.iterator
                while(billboards.hasNext) {
                    db.beginTx.closeAfter { tx =>
                        billboards.take(1000).foreach { b => counters.time("count") {
                            if (siteData != b.siteData) {
                                counters.incr("site_count")
                                siteData = b.siteData
                                site = db.createNode(SITE)
                                site.setProperty("siteID", if (siteData._1 > 0) siteData._1 else fakeID.next )

                                val c = Array(b.longitude.toMercator, b.latitude.toMercator)

                                site.setProperty(LONGITUDE, c(0))
                                site.setProperty(LATITUDE, c(1))
                                val utm = wgs2utm(c)
                                site.setProperty(LONGITUDE_UTM, utm(0))
                                site.setProperty(LATITUDE_UTM, utm(1))
                            }

                            val constrType = new Label { override def name(): String = b.gid.substring(2, 4)}
                            site.addLabel(constrType)
                            val node = db.createNode(BILLBOARD, constrType)
                            b.propertiesAsPairs foreach { x: (String, Any) => node.setProperty(x._1, x._2) }
                            node.createRelationshipTo(site, HANGING_ON)
                        }}
                    }
                    counters.print("count")
                }
        }
    }
}
