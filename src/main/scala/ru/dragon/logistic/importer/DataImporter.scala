package ru.dragon.logistic.importer

import com.twitter.logging.Logger
import com.twitter.logging.config.{ConsoleHandlerConfig, LoggerConfig}
import org.neo4j.graphdb._
import ru.dragon.logistic.GraphDB
import ru.dragon.logistic.helpers.Stats

/**
 * @author dragon
 */
object DataImporter extends App {
    implicit val db = GraphDB()

    if (args.isEmpty) {
        println("options: -importBillboards -importRoads")
    } else {
        if (args.contains("-importBillboards")) {
            BillboardImporter.run
            BillboardImporter.counters.printAll
        }

        if (args.contains("-importRoads")) {
            RoadImporter.run
            RoadImporter.counters.printAll
        }
    }

}
