package ru.dragon.logistic.importer


import com.vividsolutions.jts.geom.{Coordinate, MultiLineString}
import org.neo4j.graphdb._
import ru.dragon.logistic.GraphObjects
import ru.dragon.logistic.helpers.{ShapeFileHelper, GraphWorker, Stats}
import scala.collection.mutable
import org.opengis.feature.simple.SimpleFeature
import org.opengis.feature.`type`.AttributeDescriptor

/**
 * @author NIzhikov
 */
object RoadImporter extends GraphWorker with GraphObjects with ShapeFileHelper {
    val counters = Stats("road_importer")

    override def run()(implicit db: GraphDatabaseService) = counters.time("full_time") {
        val roads = shapeIterator("file://" + ROOT + "/ArcView/trline.shp")
        implicit val attributes = roads.attributes

        while(roads.hasNext) {
            db.beginTx.closeAfter {
                tx => roads.take(10000).foreach(importRoad)
            }
            counters.print("road")
        }

        counters.time("create_index") {
            execute("CREATE INDEX ON :GEO_POINT(latitude)")
            execute("CREATE INDEX ON :GEO_POINT(longitude)")
            execute("CREATE INDEX ON :GEO_POINT(latitude_utm)")
            execute("CREATE INDEX ON :GEO_POINT(longitude_utm)")
            execute("CREATE INDEX ON :GEO_POINT(latitude_utm_r)")
            execute("CREATE INDEX ON :GEO_POINT(longitude_utm_r)")
        }
    }

    def importRoad(road: SimpleFeature)(implicit db: GraphDatabaseService, ad: Iterable[AttributeDescriptor]) = counters.time("road") {
        val coordinates = road.coordinates
        val first = coordinates.head
        val last = coordinates.last
        val rest = coordinates.slice(1, coordinates.size-1)

        def createPoint(c: Coordinate) = db.createNode(POINT, GEO_POINT).setCoordinate(c)

        val firstNode = ifNotExists(first, road("F_ZLEV").asInstanceOf[Int]){ createPoint(first) }
        val lastNode = ifNotExists(last, road("T_ZLEV").asInstanceOf[Int]){ createPoint(last) }

        val edge = firstNode.createRelationshipTo(lastNode, ROAD)
        copyAttributes(edge, road)

        if (!road.isOneWay) {
            val re = lastNode.createRelationshipTo(firstNode, REVERSE_ROAD)
            re.setRoad(edge)
        }

        val lastGeoPoint = rest.foldLeft(firstNode) { case (node, geoPoint) =>
            val geoPointNode = db.createNode(GEO_POINT)
            geoPointNode.setCoordinate(geoPoint)

            val geoRoad = node.createRelationshipTo(geoPointNode, GEO_ROAD)
            geoRoad.setRoad(edge)

            if (!road.isOneWay) {
                val reverseGeoRoad = geoPointNode.createRelationshipTo(node, GEO_ROAD)
                reverseGeoRoad.setRoad(edge)
            }

            geoPointNode
        }

        val geoRoad = lastGeoPoint.createRelationshipTo(lastNode, GEO_ROAD)
        geoRoad.setRoad(edge)
        if (!road.isOneWay) {
            val reverseGeoRoad = lastNode.createRelationshipTo(lastGeoPoint, GEO_ROAD)
            reverseGeoRoad.setRoad(edge)
        }
    }

    val existing = mutable.HashMap[(Coordinate, Int), Long]()
    def ifNotExists(c: Coordinate, zindex: Int)(creater: =>Node)(implicit db: GraphDatabaseService): Node =
        if (existing.contains(c -> zindex)) {
            counters.incr("crossroads")
            db.getNodeById(existing(c-> zindex))
        } else {
            val node = creater
            existing(c -> zindex) = node.getId
            node
        }

    def copyAttributes(r: Relationship, f: SimpleFeature, skip: List[String] = List.empty)(implicit ad: Iterable[AttributeDescriptor]) ={
        val _skip = "the_geom" :: skip
        ad.filter(x => !_skip.contains(x.getLocalName)).foreach { a =>
            r.setProperty(a.getLocalName, f(a.getName))
        }
    }
}
