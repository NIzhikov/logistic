package ru.dragon.logistic.searcher

import java.io.FileWriter

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import ru.dragon.logistic.{GraphDB, PathSearcher}
import ru.dragon.logistic.helpers.{Using}

import scala.collection.immutable.TreeSet

/**
 * @author NIzhikov
 */

@RunWith(classOf[JUnitRunner])
class PathSearchSuite extends FunSuite with PathSearcher with Using {
    test("search ALL from Moscow center") {
        val center = (37.609218, 55.753559)
        val maxDistance = 1000
        implicit val db = GraphDB()

        val findedGIDS = db.beginTx.closeAfter { tx =>
            findBest(_ => true)(center, maxDistance).foldLeft(TreeSet[String]()) { (gids, p) =>
                val billboards = p.endNode.relationships(HANGING_ON)
                gids ++ billboards.foldLeft(Set[String]()) { (newGIDs, hangingOn) =>
                    assert(hangingOn.isType(HANGING_ON))
                    assert(hangingOn.getStartNode()("gid") != null)
                    newGIDs + hangingOn.getStartNode()("gid").asInstanceOf[String]
                }
            }
        }

        assert(TreeSet("MSCF05669Б", "MSBB06685Б", "MSCF00028А3", "MSCF05393А", "MSCF05670А2", "MSCF05835А3",
            "MSCF05409А", "MSBB10301А3", "MSCF05394Б", "MSCF05397Б", "MSCF05645С", "MSCF05394А",
            "MSCF05669А2",  "MSCF05775А3", "MSCF05041А", "MSCF05670А1", "MSCF05669А1", "MSCF05775Б", "MSCF00028А2", "MSCF00397А1",
            "MSCF05393Б", "MSCF05835А2", "MSCF05104Б", "MSBB10301А1", "MSCF05040А", "MSCF00028Б", "MSBB10301А2", "MSCF05645Б",
            "MSCF00397Б", "MSCF05041Б", "MSCF05407Б", "MSCF00397А2", "MSCF05409Б", "MSCF05104А", "MSCF05040Б",
            "MSCF00028А1", "MSBB10300А3", "MSCF05835А1", "MSCF05758Б", "MSBB10300А1", "MSCF05645А",
            "MSCF05670Б", "MSCF05407А", "MSCF05408Б", "MSCF05670А3", "MSBB10300А2", "MSCF00397А3", "MSCF05775А2",
            "MSCF05408А", "MSCF05775А1", "MSCF05397А", "MSCF05835Б", "MSCF05758А1", "MSCF05669А3", "MSCF05758А2",
            "MSCF05758А3", "MSCF03489А1", "MSCF03489А2", "MSCF05409А", "MSCF05409Б", "MSCF05110А", "MSCF05110Б") === findedGIDS)
    }

    test("search 6x3(BB) from Moscow center") {
        val center = (37.609218, 55.753559)
        val maxDistance = 1000
        implicit val db = GraphDB()

        val findedGIDS = db.beginTx.closeAfter { tx =>
            findBest(constructionTypePredicate("BB"))(center, maxDistance).
                foldLeft(Set[String]()) { (gids, p) =>
                    val billboards = p.endNode.relationships(HANGING_ON)
                    gids ++ billboards.foldLeft(Set[String]()) { (newGIDs, hangingOn) =>
                        assert(hangingOn.isType(HANGING_ON))
                        assert(hangingOn.getStartNode()("gid").asInstanceOf[String].contains("BB"))
                        newGIDs + hangingOn.getStartNode()("gid").asInstanceOf[String]
                    }
                }
        }

        assert(Set("MSBB06685Б", "MSBB10301А3", "MSBB10301А1", "MSBB10301А2",
            "MSBB10300А3", "MSBB10300А1", "MSBB10300А2") === findedGIDS)
    }

    test("search best path for MSBB02550") {
        val center = (37.473433850828144, 55.72841676718632)
        val maxDistance = 1000
        implicit val db = GraphDB()

        val findedGIDS = db.beginTx.closeAfter { tx =>
            findBest{n => n("gid").toString.startsWith("MSBB02550") }(center, maxDistance).
                foldLeft(Set[String]()) { (gids, p) =>

                //Дороги по которым проходит путь к поверхности.
                val roads = p.allRelationships.filter(_.isType(GEO_ROAD)).map(_.road).toList.distinct
                //Должен проходить только по Кутузовскому - т.к. это главная дорога
                //Хотя кратчайший путь будет, если свернуть на улицу, которая идет к м. Славянский бульвар
                assert(roads.length === 2)
                assert(roads(0)("OBJ_COD") === 388)
                assert(roads(1)("OBJ_COD") === 388)

                val billboards = p.endNode.relationships(HANGING_ON)
                gids ++ billboards.foldLeft(Set[String]()) { (newGIDs, hangingOn) =>
                    assert(hangingOn.isType(HANGING_ON))
                    assert(hangingOn.getStartNode()("gid").asInstanceOf[String].contains("BB"))
                    newGIDs + hangingOn.getStartNode()("gid").asInstanceOf[String]

                }
            }
        }

        assert(Set("MSBB02550Б", "MSBB02550А") === findedGIDS)
    }

    test("search best path for MSBB01240") {
        val center = (37.473433850828144, 55.72841676718632)
        val maxDistance = 1000
        implicit val db = GraphDB()

        val findedGIDS = db.beginTx.closeAfter { tx =>
            findBest{n => n("gid").toString.startsWith("MSBB01240") }(center, maxDistance).
                foldLeft(Set[String]()) { (gids, p) =>

                //Дороги по которым проходит путь к поверхности.
                val roads = p.allRelationships.filter(_.isType(GEO_ROAD)).map(_.road).toList.distinct
                //Должен проходить только по Кутузовскому - т.к. это главная дорога
                //Хотя кратчайший путь будет, если свернуть на улицу, которая идет к м. Славянский бульвар
                assert(roads.length === 2)
                assert(roads(0)("OBJ_COD") === 388)
                assert(roads(1)("OBJ_COD") === 388)

                val billboards = p.endNode.relationships(HANGING_ON)
                gids ++ billboards.foldLeft(Set[String]()) { (newGIDs, hangingOn) =>
                    assert(hangingOn.isType(HANGING_ON))
                    assert(hangingOn.getStartNode()("gid").asInstanceOf[String].contains("BB"))
                    newGIDs + hangingOn.getStartNode()("gid").asInstanceOf[String]

                }
            }
        }

        assert(Set("MSBB01240А", "MSBB01240Б") === findedGIDS)
    }

    test("search should go to road junction on minskaya and kutuzovsky") {
        val center = (37.49130809170416, 55.7308263807183)
        val maxDistance = 1500
        implicit val db = GraphDB()

        val findedGIDS = db.beginTx.closeAfter { tx =>
            findBest{n => true }(center, maxDistance).
                foldLeft(Set[String]()) { (gids, p) =>
                val billboards = p.endNode.relationships(HANGING_ON)
                gids ++ billboards.foldLeft(Set[String]()) { (newGIDs, hangingOn) =>
                    assert(hangingOn.isType(HANGING_ON))
                    newGIDs + hangingOn.getStartNode()("gid").asInstanceOf[String]
                }
            }
        }

        assert(findedGIDS("MSBB06669Б"), "MSBB06669Б")
        assert(findedGIDS("MSBB01079А"))
        assert(findedGIDS("MSBB01079Б"))
    }

    test ("don't use forbidden turn from sadovoe to prospekt mira") {
        val center = (37.629989026609394, 55.772922068763116)
        val maxDistance = 1500
        implicit val db = GraphDB()

        db.beginTx.closeAfter { tx =>
            findBest{n => n("gid").toString.startsWith("MSBB06675") }(center, maxDistance).foreach { p =>
                //Дороги по которым проходит путь к поверхности.
                val roads = p.allRelationships.filter(_.isType(GEO_ROAD)).map(_.road).toList.distinct
                roads.tail.foldLeft(roads.head) { (prev, cur) =>
                    assert(!(prev("OBJ_COD")==3650 && cur("OBJ_COD")==1055), "Turn from sadovoe inner to prospekt mira is forbidden")// 3650 - внутреннее садовое кольцо. 1055 - проспект Мира. Такой поворот запрещен.
                    cur
                }

                println(p.gids.mkString("(", ", ", ")") + ": " + p.allRelationships.filter(_.isType(GEO_ROAD)).map(_.road).map(_("OBJ_COD")).toList.distinct.mkString("-"))
            }
        }
    }
/*
    test("very slow search in Moscow center") {
        val center = (37.60063493115126, 55.76749702782299)
        val maxDistance = 3000
        implicit val db = GraphDB()

        val findedGIDS = db.beginTx.closeAfter { tx =>
            findBest{n => true }(center, maxDistance).
                foldLeft(Set[String]()) { (gids, p) =>
                val billboards = p.endNode.relationships(HANGING_ON)
                gids ++ billboards.foldLeft(Set[String]()) { (newGIDs, hangingOn) =>
                    assert(hangingOn.isType(HANGING_ON))
                    newGIDs + hangingOn.getStartNode()("gid").asInstanceOf[String]
                }
            }
        }

        assert(findedGIDS.nonEmpty)
        counters.printAll
    }
*/
}
