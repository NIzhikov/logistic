package ru.dragon.logistic.helpers

import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import org.junit.runner.RunWith


/**
 * @author NIzhikov
 */
@RunWith(classOf[JUnitRunner])
class CoordinateConverterSuite extends FunSuite with CoordinateConverter {
    val MOSCOW_UTM1 = Array(413106.987, 6179370.748)
    val MOSCOW_WGS1 = Array(37.615560, 55.752220)

    val MOSCOW_UTM2 = Array(413284.503, 6179258.552)
    val MOSCOW_WGS2 = Array(37.618423, 55.751244)

    test ("Moscow wgs -> utm") {
        assert(wgs2utm(MOSCOW_WGS1) === MOSCOW_UTM1)
    }

    test ("Moscow2 wgs -> utm") {
        assert(wgs2utm(MOSCOW_WGS2) === MOSCOW_UTM2)
    }

    test ("Moscow utm -> wgs") {
        assert(utm2wgs(MOSCOW_UTM1) === MOSCOW_WGS1)
    }

    test ("Moscow2 utm -> wgs") {
        assert(utm2wgs(MOSCOW_UTM2) === MOSCOW_WGS2)
    }
}