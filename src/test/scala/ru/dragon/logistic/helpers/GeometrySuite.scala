package ru.dragon.logistic.helpers

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

/**
 * @author NIzhikov
 */
@RunWith(classOf[JUnitRunner])
class GeometrySuite extends FunSuite with Geometry {
    def testTriangle(a: (Double, Double), b: (Double, Double), c: (Double, Double))
                    (x: (Double, Double), h: Double) = {
        val n = normal(Array(a._1, a._2), Array(b._1, b._2), Array(c._1, c._2))
        assert(n.coordinateUTM === Array(x._1, x._2))
        assert(n.length === h)
    }

    test ("Simplest X triangle") { testTriangle((0, 0), (4, 0), (2, 2))((2, 0), 2) }

    test ("Simplest Y triangle") { testTriangle((0, 0), (0, 4), (2, 2)) ((0, 2), 2) }

    test ("Simple triangle 1") { testTriangle((4, 0), (0, 4), (0, 0)) ((2, 2), round(Math.sqrt(8), 3)) }

    test ("Simple triangle 2") { testTriangle((2, 0), (4, 0), (0, 3)) ((0, 0), 3) }

    test ("Simple triangle 3") { testTriangle((2, 0), (4, 0), (6, 3)) ((6, 0), 3) }

    test ("Simple triangle 4") { testTriangle((0, 2), (0, 4), (3, 6)) ((0, 6), 3) }

    test ("Triangle 1") { testTriangle((4, 4), (8, 2), (9, 8)) ((6.4, 2.8), 5.814) }

    test ("Triangle 2") { testTriangle((4, 4), (8, 2), (3, -3)) ((6, 3), 6.708) }

    test ("Triangle 3") { testTriangle((5, 2), (7, 6), (1, 7)) ((6.2, 4.4), 5.814) }

    test ("Triangle 4") { testTriangle((5, 2), (7, 6), (3, 10)) ((7.8, 7.6), 5.367) }

    test ("Triangle 5") { testTriangle((5, 2), (7, 6), (1, 1)) ((3.8, -.4), 3.13) }

    test ("Triangle 6") { testTriangle((5, 2), (7, 6), (6, 9)) ((8, 8), round(Math.sqrt(5), 3)) }

    test ("Simplest divide road 1") {
        assert(divideRoad(1)(Array(0d, 0d), Array(0d, 3d)) === Array(Array(0, 1), Array(0, 2)))
    }

    test ("Simplest divide road 2") {
        assert(divideRoad(1)(Array(0d, 0d), Array(3d, 0d)) === Array(Array(1, 0), Array(2, 0)))
    }

    test ("Divide road 1") {
        assert(divideRoad(Math.sqrt(2))(Array(0d, 0d), Array(3d, 3d)) === Array(Array(1, 1), Array(2, 2)))
    }

    test ("Divide road 1 - reverse") {
        assert(divideRoad(Math.sqrt(2))(Array(3d, 3d), Array(0d, 0d)) === Array(Array(2, 2), Array(1, 1)))
    }

    test ("Divide road 2") {
        assert(divideRoad(Math.sqrt(2))(Array(0d, 3d), Array(3d, 0d)) === Array(Array(1, 2), Array(2, 1)))
    }

    test ("Divide road 2 - reverse") {
        assert(divideRoad(Math.sqrt(2))(Array(3d, 0d), Array(0d, 3d)) === Array(Array(2, 1), Array(1, 2)))
    }

    test ("Nothing to divide") {
        assert(divideRoad(Math.sqrt(2))(Array(0d, 0d), Array(0d, 1d)) === Array())
    }


    test ("isInside 1") {
        assert(isInside(Array(0d, 0d), Array(0d, 2d), Array(0d, 1d)) === true)
    }

    test ("isInside 2") {
        assert(isInside(Array(0d, 0d), Array(2d, 2d), Array(1d, 1d)) === true)
    }
}
